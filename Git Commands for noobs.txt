Check status:
	git fetch (server status)
	git status (client status)

to pull changes:
	git pull

to push changes:
	git add -A
	git commit -m "(comment here)"
	git pull
	git push

to discard all changes:
	git checkout -- .

to discard specific changes:
	git checkout (filename)
