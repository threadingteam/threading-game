﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace Threading_Game
{
    class House : GameObject
    {

        #region Fields
        private Texture2D sprite;
        private Vector2 position;
        private SpriteFont spriteFont;
        private List<Worker> worker;
        private int gold;
        private Thread houseThread;
        private Object thisLock = new Object();
        #endregion

        #region Properties
        public int Gold
        {
        
        set
            {
                Monitor.Enter(thisLock);// a  monitor lock to make sure that if anything occurs, the program will stil function even if 1 thread suddenly dies.
                try
                {
                    gold = value;
                }
                finally
                {
                    Monitor.Exit(thisLock);
                }
            }

            get
            {
                lock (thisLock) // standard lock
                {
                    return gold;
                }
            }
        }
        #endregion

        #region Constructors
        public House(List<Worker> worker)//Takes the worker list to check the workers positions in he Update() and instantiates the thread to run Update()
        {
            this.worker = worker;
            this.position = new Vector2(50, 100);
            houseThread = new Thread(Update);
            houseThread.IsBackground = true;
            houseThread.Start();
        }
        #endregion

        #region Methods
        public override void LoadContent(ContentManager content)//Loads the sprite and the spritefont to draw text too
        {
            sprite = content.Load<Texture2D>("House");
            spriteFont = content.Load<SpriteFont>("SpriteFont");
        }


        private void Update()//checks the position of each worker, and if the position overlaps with house the coin bool is set to false
        {
            while (houseThread.IsAlive)
            {
                foreach (var item in worker)
                {
                    //if workers position is at house take coin and chance bool
                    if (item.Position.X <= 96 && item.Coin == true)
                    {
                        Thread.Sleep(1000);
                        gold += 2;
                        item.Coin = false;
                    }

                }
                Thread.Yield();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)//draws the house and the amount of coins stored in the int
        {
            spriteBatch.Draw(sprite, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            spriteBatch.DrawString(spriteFont, "Coins: " + gold, position + new Vector2(0, -16), Color.Gold);
        }
        
        #endregion
    }
}
