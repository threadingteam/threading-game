﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace Threading_Game
{
    class Mine : GameObject
    {

        #region Fields
        private Texture2D sprite;
        private Vector2 position;
        private List<Worker> worker;
        private Thread mineThread;
        #endregion
        
        #region Constructors
        public Mine(List<Worker> worker)//takes a worker list as parameter to test whether to add to the coin int, furthermore it instantiates the thread
        {
            this.worker = worker;
            this.position = new Vector2(400, 100);
            mineThread = new Thread(Update);
            mineThread.IsBackground = true;
            mineThread.Start();
        }
        #endregion

        #region Methods
        public override void LoadContent(ContentManager content)//Loads the Mine sprite
        {
            sprite = content.Load<Texture2D>("Mine");
        }


        private void Update() //Checks for the worker position and if the position overlaps with the mine sprite, if thats the case the bool coin will be set true
        {
            while (mineThread.IsAlive)
            {
                foreach (var item in worker)
                {
                    if (item.Position.X >= 400)
                    {
                        Thread.Sleep(1000);
                        item.Coin = true;
                    }
                }
                Thread.Yield();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)//draws the Mine sprite
        {
            spriteBatch.Draw(sprite, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
        }

        #endregion
        
    }
}
