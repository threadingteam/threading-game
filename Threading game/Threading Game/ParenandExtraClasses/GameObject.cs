﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Threading_Game
{
    abstract class GameObject 
    {
        #region Fields
        private Texture2D sprite;
        private Vector2 position;
        #endregion
        
        #region Constructer
        public GameObject ()
        {
            
        }
        #endregion
        
        #region Methods
        public abstract void LoadContent(ContentManager content);
        
        public abstract void Draw(SpriteBatch spriteBatch);

        #endregion
    }
}
