﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace Threading_Game
{
    class Worker : GameObject
    {


        #region Fields
        private Texture2D workerSprite, coinSprite;
        private Vector2 position;
        private SpriteFont spriteFont;
        private bool coin = false, active = false;
        private Thread workThread;
        private string status;
        #endregion

        #region Properties
        //allows acces to the coin boolean from outside
        public bool Coin
        {
            get
            {
                return coin;
            }
            set
            {
                coin = value;
            }
        }
        //allows readonly acces the Vector2 position from outside
        public Vector2 Position
        {
            get
            {
                return position;
            }
        }
        //allows acces to the active boolean from outside
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }
        #endregion

        #region Constructors
        public Worker()//sets workers position relative to house and initializes the thread that runs Update()
        {
            this.position = new Vector2(96, 146);
            workThread = new Thread(Update);
            workThread.IsBackground = true;
            workThread.Start();
        }
        #endregion

        #region Methods
        public override void LoadContent(ContentManager content) //Loads necessary textures into the object
        {
            workerSprite = content.Load<Texture2D>("Worker");
            coinSprite = content.Load<Texture2D>("Coin");
            spriteFont = content.Load<SpriteFont>("SpriteFont");
        }


        private void Update()//the logic behind the movement of workers and their timings to move smoothly
        {
            //auto walk
            while (workThread.IsAlive) //only loops while the thread is alive.
            {
                if (active)//the code only runs if the object is activated fro outside
                {

                    if (!coin && position.X <= 405) //Moves the worker from left to right and updates the status string
                    {
                        position.X += 3;
                        status = "Moving to Mine";
                    }
                    else if (coin && position.X >= 96) //Moves the worker from right to left and updates the status string
                    {
                        position.X -= 1.5f;
                        status = "Moving to House";
                    }

                    if (Position.X <= 96 && coin)//checks to see if worker is on top of house with a coin to drop
                    {
                        status = "Dropping coins";
                    }

                    if (!coin && position.X >= 400)//checks to see if worker is on top of the mine and the worker doesnt have a coin
                    {
                        status = "Mining coins";
                        Thread.Sleep(1000);
                    }
                    Thread.Sleep(17); //this makes sure that loop only happens 60 times a second to ensure a smmoth experience
                }
                else //the else statements make sure that the thread, if inactive, wont loop needlessly
                {
                    status = "Inactive";
                    Thread.Yield();
                }
            }
        }


        public override void Draw(SpriteBatch spriteBatch)//Draws all the sprites loaded in earlier on top of the coordiates of the object
        {
            if (active)
            {
                spriteBatch.Draw(workerSprite, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.1f);
                //if helding coin: draw coin + font telling how much worker got

                if (coin)//only draws if the worker carries a coin
                {
                    spriteBatch.Draw(coinSprite, position + new Vector2(3.2f, -7), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    spriteBatch.DrawString(spriteFont, "2", position + new Vector2(2, -23), Color.Gold);
                }
            }
        }

        public void Status(SpriteBatch spriteBatch, Vector2 position)//used to display the status top-right in the game
        {
            spriteBatch.DrawString(spriteFont, "Worker: " + status, position, Color.White);
        }
        #endregion

    }
}
