﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace Threading_Game
{
    class Bank : GameObject
    {


        #region Fields
        private Texture2D sprite;
        private SpriteFont spriteFont;
        private Vector2 position;
        private int bankGold = 0;
        private readonly Object thisLock = new object();
        private Mutex myMutex = new Mutex();
        #endregion

        #region Properties
        public Vector2 BankPosition
        {
            get
            {
                lock (thisLock)
                {
                    return position;
                }
            }
        }

        public int BankGold
        {
            get
            {
                lock (thisLock)
                {
                    return bankGold;
                }
            }

            set
            {
                myMutex.WaitOne();

                bankGold = value;

                myMutex.ReleaseMutex();
            }
        }

        #endregion

        #region Constructors
        public Bank()//sets the position, doesnt instantiate a thread since an Update methods isn't necessary
        {
            this.position = new Vector2(50, 350);
        }
        #endregion

        #region Methods
        public override void LoadContent(ContentManager content)//Loader spriteFont to write text alongside the sprite
        {
            spriteFont = content.Load<SpriteFont>("SpriteFont");
            sprite = content.Load<Texture2D>("Bank");
        }


        public override void Draw(SpriteBatch spriteBatch) // draws the sprite and text
        {
            //draw bank building + font telling howmuch gold bank got
            spriteBatch.Draw(sprite, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            spriteBatch.DrawString(spriteFont, "Gold: " + bankGold, position + new Vector2(0, -16), Color.Gold);
        }

        #endregion

    }
}
