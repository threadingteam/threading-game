﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Threading;

namespace Threading_Game
{
    class Banker : GameObject
    {

        #region Fields
        private Texture2D bankerSprite;
        private Texture2D coinSprite;
        private Vector2 position;
        private bool coin;
        private Bank bank;
        private SpriteFont spriteFont;
        private House house;
        private Thread bankerThread;
        private string status;
        #endregion

        #region Constructors
        //takes info from house and bank classes to enable coin transfers
        //also sets position relative to the bank and initializes the thread
        public Banker(House house, Bank bank)
        {
            this.house = house;
            this.bank = bank;
            this.position = bank.BankPosition + new Vector2(46, 46);
            bankerThread = new Thread(Update);
            bankerThread.IsBackground = true;
            bankerThread.Start();

        }
        #endregion

        #region Methods
        public override void LoadContent(ContentManager content)
        {
            bankerSprite = content.Load<Texture2D>("Banker");
            coinSprite = content.Load<Texture2D>("Coin");
            spriteFont = content.Load<SpriteFont>("SpriteFont");
        }


        private void Update()
        {
            while (bankerThread.IsAlive)//only runs as long as the thread is active
            {
                //auto walk
                if (house.Gold < 5)//only active if the house has a minimum of 5 coins
                {
                    status = "Waiting on miners";
                }
                if (house.Gold >= 5 || coin)
                {
                    if (!coin)
                    {
                        position.Y -= 3;
                        status = "Moving to House";
                        //checks if reached destination then gives coin
                        if (!coin && position.Y <= 144)
                        {
                            status = "Getting Coins";
                            Thread.Sleep(1000);
                            house.Gold -= 5;
                            coin = true;
                            Thread.Yield();
                        }
                    }
                    else if (coin && position.Y <= 396)
                    {
                        position.Y += 0.5f;
                        status = "Moving to Bank";
                        if (position.Y >= 396)//checks if reached destination then removes coins + gives Bank coins
                        {
                            status = "Unloading coins";
                            Thread.Sleep(1000);
                            coin = false;
                            bank.BankGold += 5;
                        }
                    }

                    Thread.Sleep(17);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)//draws the sprites
        {
            spriteBatch.Draw(bankerSprite, position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.1f);
            //if holding coin: draw coin + font telling how much banker got
            if (coin)
            {
                spriteBatch.Draw(coinSprite, position + new Vector2(3.2f, -7), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(spriteFont, "5", position + new Vector2(2, -23), Color.Gold);
            }
        }

        public void Status(SpriteBatch spriteBatch, Vector2 position)//necessary to print the status of the banker top-right
        {
            spriteBatch.DrawString(spriteFont, "Banker: " + status, position, Color.White);
        }
        #endregion
    }
}
