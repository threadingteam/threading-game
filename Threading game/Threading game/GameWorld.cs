﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Threading_Game
{

    public class GameWorld : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private List<GameObject> gameObjects; //Only for Gameobjects other than Worker objects
        private List<Worker> workerObjects; //List for workers only
        private int bankers = 1, workers = 1;
        private Vector2 statusPos, statusPosReset; //these positions are used to draw the status text top-right

        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize() //Initializes all the objects at once, except the Banker class
        {
            //Add initialization logic here
            gameObjects = new List<GameObject>();
            workerObjects = new List<Worker>();

            for (int i = 0; i < 5; i++)//adds all the worker classes at once, to work around a bug we had
            {
            workerObjects.Add(new Worker());
            }

            gameObjects.Add(new Mine(workerObjects));
            gameObjects.Add(new House(workerObjects));
            gameObjects.Add(new Bank());

            statusPosReset = new Vector2(615, 0);//sets the standard position of the text top-right
            
            base.Initialize();
        }

        protected override void LoadContent()//cycles through all objects and calls their respective loadContent() methods
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            foreach (var item in gameObjects)
            {
                item.LoadContent(Content);
            }

            foreach (var item in workerObjects)
            {
                item.LoadContent(Content);
            }
        }

        protected override void UnloadContent()//not used in this game
        {
        }

        protected override void Update(GameTime gameTime)//the main Update function that spawns in new objects when needed
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (!workerObjects[0].Active)
            {
                workerObjects[0].Active = true;
            }
            if ((gameObjects[1] as House).Gold >= 5 * bankers)
            {
                bankers += 1;
                gameObjects.Add(new Banker((House)gameObjects[1], (Bank)gameObjects[2]));
                LoadContent();
            }

            if ((gameObjects[2] as Bank).BankGold >= 4 + (2 * workers))
            {
                if (workers < 5)
                {
                    (gameObjects[2] as Bank).BankGold -= 6;
                    workerObjects[workers].Active = true;
                    workers += 1;
                }
            }
            

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)//draws all the objects on the screen
        {
            GraphicsDevice.Clear(Color.ForestGreen);//clears the screen

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);//makes the layerdepth work if it's needed
            
            statusPos = statusPosReset;//resets the postition for the text top-right after the calculations in both foreach loops
            statusPos.Y += 75;
            foreach (var item in gameObjects)
            {
                item.Draw(spriteBatch);
                if(ReferenceEquals(item, (item as Banker)))
                {
                    (item as Banker).Status(spriteBatch, statusPos);
                    statusPos.Y += 15;
                }
            }

            statusPos = new Vector2(615, 0);
            foreach (Worker item in workerObjects)
            {
                item.Draw(spriteBatch);
                item.Status(spriteBatch, statusPos);
                statusPos.Y += 15; //spaces the text properly
            }
            
            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
